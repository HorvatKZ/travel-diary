import { navState, screen2lnglat } from './navigation.js'

export function getHoveredCountry(countries) {
    const mouseCoords = screen2lnglat(navState.mouse)
    for (let country of countries) {
        if (isPointInCountry(mouseCoords, country)) {
            return country.code
        }
    }

    return ''
}


function isPointInCountry(point, country) {
    if (point[0] < country.limits.min_lng || point[0] > country.limits.max_lng ||
        point[1] < country.limits.min_lat || point[1] > country.limits.max_lat) {
        return false
    }

    for (let poly of country.coords) {
        if (isPointInPolygon(point, poly)) {
            return true
        }
    }

    return false
}


function isPointInPolygon(point, vertices) {
    // https://stackoverflow.com/a/72434617
    const x = point[0]
    const y = point[1]
    
    let inside = false
    for (let i = 0, j = vertices.length - 1; i < vertices.length; j = i++) {
        const xi = vertices[i][0]
        const yi = vertices[i][1]
        const xj = vertices[j][0]
        const yj = vertices[j][1]
    
        const intersect = yi > y != yj > y && x < ((xj - xi) * (y - yi)) / (yj - yi) + xi
        if (intersect) {
            inside = !inside
        }
    }
    
    return inside
}