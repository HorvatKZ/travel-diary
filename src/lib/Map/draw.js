import { get } from 'svelte/store'

import { navState, lnglat2screen, screen2lnglat } from './navigation.js'
import { getHoveredCountry } from './select.js'
import { lastHoveredCountry } from './stores.js'


export const drawCfg = {
    colors: {
        // Source: https://flatuicolors.com/palette/defo
        water: '#3498db',
        ground: '#27ae60',
        hovered: '#2ecc71',
        arctic: '#ecf0f1',
        hoveredArtcic: '#bdc3c7',
        river: '#2980b9',
        border: '#2c3e50',
        city: '#d35400',
        cityName: '#ecf0f1',
        lines: '#487eb0'
    },
    size: {
        border: 1.0,
        river: 0.05,
        capital: [ 0.5, 0.3, 0.15, 0.15, 0.15 ],
        capitalName: [ 18, 15, 12, 12, 12 ],
        city: [ 0.5, 0.3, 0.15, 0.07, 0.03 ],
        cityName: [ 18, 15, 12, 11, 10 ],
        line: 20
    },
    limits: {
        river: 5,
        lake: 5,
        capital: 8,
        city: [ 8, 8, 30, 70, 200 ]
    },
    scale: {
        bg: 'rgba(255, 255, 255, 0.7)',
        fg: 'black',
        lineWidth: 2,
        lineHeight: 8,
        textSize: 15,
        border: 5,
        shift: 10,
        possibleScales: [ 5000, 2000, 1000, 500, 200, 100, 50, 20, 10, 5, 2, 1 ],
        maxLength: 200
    },
    populationClasses: [ 5_000_000, 1_000_000, 150_000, 30_000 ],
    lineFrequencies: [ 45, 30, 15, 5, 2, 1 ],
    detailLimit: 20
}


export function drawLoading(ctx) {
    ctx.fillText('Loading...', navState.w / 2, navState.h / 2)
}


export function draw(ctx, mapData, forceRedraw) {
    const hoveredCountry = getHoveredCountry(getLODComponent(mapData, 'countries'))
    if (!forceRedraw && hoveredCountry == get(lastHoveredCountry)) {
        return
    }
    lastHoveredCountry.set(hoveredCountry)

    beginDraw(ctx)

    for (let country of getLODComponent(mapData, 'countries')) {
        const isHovered = country.code === hoveredCountry
        drawCountry(ctx, country, true, isHovered)
    }

    if (navState.scale > drawCfg.limits.river) {
        for (let river of getLODComponent(mapData, 'rivers')) {
            drawRiver(ctx, river)
        }
    }

    if (navState.scale > drawCfg.limits.lake) {
        for (let lake of getLODComponent(mapData, 'lakes')) {
            drawLake(ctx, lake)
        }
    }

    for (let country of getLODComponent(mapData, 'countries')) {
        drawCountry(ctx, country, false, false)
    }

    drawLngLatLines(ctx)

    for (let city of mapData.cities) {
        drawCity(ctx, city)
    }
    for (let city of mapData.cities) {
        drawCityName(ctx, city)
    }

    drawScale(ctx)
}


function getLODComponent(mapData, name) {
    if (navState.scale > drawCfg.detailLimit && mapData[name + 'Detailed']) {
        return mapData[name + 'Detailed']
    } else {
        return mapData[name]
    }
}


function beginDraw(ctx) {
    ctx.clearRect(0, 0, navState.w, navState.h)
    ctx.fillStyle = drawCfg.colors.water
    ctx.fillRect(0, 0, navState.w, navState.h)
}


function drawCountry(ctx, country, makeFill, isHovered) {
    if (!isAreaOnScreen(country.limits)) {
        return
    }

    ctx.fillStyle = getCountryColor(country, isHovered)
    ctx.strokeStyle = drawCfg.colors.border
    ctx.lineWidth = drawCfg.size.border
    for (let poly of country.coords) {
        drawPolygon(ctx, poly, makeFill)
    }
}


function getCountryColor(country, isHovered) {
    if (isCountryArtctic(country)) {
        return isHovered ? drawCfg.colors.hoveredArtcic : drawCfg.colors.arctic
    }
    return isHovered ? drawCfg.colors.hovered : drawCfg.colors.ground
}


function isCountryArtctic(country) {
    return country.code == 'ATA' || country.code == 'GRL'
}


function drawRiver(ctx, river) {
    if (!isAreaOnScreen(river.limits)) {
        return
    }

    ctx.strokeStyle = drawCfg.colors.river
    ctx.lineWidth = drawCfg.size.river * navState.scale
    for (let poly of river.coords) {
        drawPolyline(ctx, poly, drawCfg.colors.river)
    }
}


function drawLake(ctx, lake) {
    if (!isAreaOnScreen(lake.limits)) {
        return
    }

    ctx.fillStyle = drawCfg.colors.water
    ctx.strokeStyle = drawCfg.colors.river
    ctx.lineWidth = drawCfg.size.border
    for (let poly of lake.coords) {
        drawPolygon(ctx, poly)
    }
}


function drawCity(ctx, city) {
    if (!isPointOnScreen(city.coords)) {
        return
    }

    const isCapital = city.level == 0
    const popClass = getPopulationClass(city.population)
    if (isCapital && navState.scale < drawCfg.limits.capital ||
        !isCapital && navState.scale < drawCfg.limits.city[popClass]) {
        return
    }

    const p = lnglat2screen(city.coords)
    ctx.fillStyle = drawCfg.colors.city
    ctx.strokeStyle = drawCfg.colors.border
    ctx.lineWidth = drawCfg.size.border
    const size = (isCapital ? drawCfg.size.capital[popClass] : drawCfg.size.city[popClass]) * navState.scale
    if (isCapital) {
        ctx.fillRect(p[0] - size, p[1] - size, 2 * size, 2 * size)
        ctx.strokeRect(p[0] - size, p[1] - size, 2 * size, 2 * size)
    } else {
        ctx.beginPath()
        ctx.arc(p[0], p[1], size, 0, 2 * Math.PI, false)
        ctx.fill()
        ctx.stroke()
    }
}


function drawCityName(ctx, city) {
    if (!isPointOnScreen(city.coords)) {
        return
    }

    const isCapital = city.level == 0
    const popClass = getPopulationClass(city.population)
    if (isCapital && navState.scale < drawCfg.limits.capital ||
        !isCapital && navState.scale < drawCfg.limits.city[popClass]) {
        return
    }

    const p = lnglat2screen(city.coords)
    const size = (isCapital ? drawCfg.size.capital[popClass] : drawCfg.size.city[popClass]) * navState.scale
    const fontSize = isCapital ? drawCfg.size.capitalName[popClass] : drawCfg.size.cityName[popClass]
    ctx.fillStyle = drawCfg.colors.cityName
    ctx.font = `bold ${fontSize}px sans-serif`
    ctx.fillText(' ' + city.name, p[0] + size, p[1] + fontSize / 3)
}


function drawPolygon(ctx, points, makeFill = true) {
    const startPoint = lnglat2screen(points[0])
    ctx.beginPath()
    ctx.moveTo(startPoint[0], startPoint[1])
    for (let point of points) {
        const p = lnglat2screen(point)
        ctx.lineTo(p[0], p[1])
    }
    ctx.closePath()
    if (makeFill) {
        ctx.fill()
    }
    ctx.stroke()
}


function drawPolyline(ctx, points, strokeStyle) {
    ctx.strokeStyle = strokeStyle
    const startPoint = lnglat2screen(points[0])
    ctx.beginPath()
    ctx.moveTo(startPoint[0], startPoint[1])
    for (let point of points) {
        const p = lnglat2screen(point)
        ctx.lineTo(p[0], p[1])
    }
    ctx.stroke()
}


function drawLngLatLines(ctx) {
    const increment = calcMapLineIncrement()
    const [ min_lng, max_lat ] = screen2lnglat([ 0, 0 ])
    const [ max_lng, min_lat ] = screen2lnglat([ navState.w, navState.h ])
    const latStart = Math.ceil(min_lat / increment) * increment
    const lngStart = Math.ceil(min_lng / increment) * increment

    ctx.strokeStyle = drawCfg.colors.lines
    ctx.fillStyle = drawCfg.colors.lines
    ctx.lineWidth = drawCfg.size.border
    ctx.font = `bold ${drawCfg.size.line}px sans-serif`
    for (let lat = latStart; lat < max_lat; lat += increment) {
        drawMapLine(ctx, [ min_lng, lat ], [ max_lng, lat ], lat)
    }
    for (let lng = lngStart; lng < max_lng; lng += increment) {
        drawMapLine(ctx, [ lng, min_lat ], [ lng, max_lat ], lng)
    }
}


function calcMapLineIncrement() {
    for (let i in drawCfg.lineFrequencies) {
        if (drawCfg.lineFrequencies[i] * navState.scale < drawCfg.lineFrequencies[0]) {
            return drawCfg.lineFrequencies[parseInt(i) - 1]
        }
    }
    return drawCfg.lineFrequencies[drawCfg.lineFrequencies.length - 1]
}


function drawMapLine(ctx, start, end, label) {
    const endpoints = [ lnglat2screen(start), lnglat2screen(end) ]
    ctx.beginPath()
    ctx.moveTo(endpoints[0][0], endpoints[0][1])
    ctx.lineTo(endpoints[1][0], endpoints[1][1])
    ctx.stroke()
    ctx.fillText(label, endpoints[0][0] + drawCfg.size.line / 4, endpoints[0][1] - drawCfg.size.line / 4)

}


function drawScale(ctx) {
    const kmScale = getKmScale()
    const length = kmToPixel(kmScale)
    const label = `${kmScale} km`

    ctx.font = `bold ${drawCfg.scale.textSize}px sans-serif`
    const labelExtent = ctx.measureText(label)
    const labelWidth = labelExtent.width
    const labelHeight = labelExtent.actualBoundingBoxAscent + labelExtent.actualBoundingBoxDescent

    const startPos = [ navState.w - length - labelWidth - 3 * drawCfg.scale.border - drawCfg.scale.shift, navState.h - drawCfg.scale.border - drawCfg.scale.shift ]
    ctx.fillStyle = drawCfg.scale.bg
    ctx.fillRect(startPos[0] - drawCfg.scale.border, startPos[1] - labelHeight - drawCfg.scale.border, length + labelWidth + 4 * drawCfg.scale.border, labelHeight + 2 * drawCfg.scale.border)

    ctx.lineWidth = drawCfg.scale.lineWidth
    ctx.strokeStyle = drawCfg.scale.fg

    ctx.beginPath()
    ctx.moveTo(startPos[0], startPos[1])
    ctx.lineTo(startPos[0] + length, startPos[1])
    ctx.stroke()

    ctx.beginPath()
    ctx.moveTo(startPos[0], startPos[1])
    ctx.lineTo(startPos[0], startPos[1] - drawCfg.scale.lineHeight)
    ctx.stroke()
    ctx.beginPath()
    ctx.moveTo(startPos[0] + length, startPos[1])
    ctx.lineTo(startPos[0] + length, startPos[1] - drawCfg.scale.lineHeight)
    ctx.stroke()

    ctx.fillStyle = drawCfg.scale.fg
    ctx.fillText(label, startPos[0] + length + 2 * drawCfg.scale.border, startPos[1])
}


function getKmScale() {
    for (let scale of drawCfg.scale.possibleScales) {
        if (kmToPixel(scale) < drawCfg.scale.maxLength) {
            return scale
        }
    }
    return drawCfg.scale.possibleScales[drawCfg.scale.possibleScales.length - 1]
}


function kmToPixel(km) {
    const min_lat = screen2lnglat([ 0, navState.h ])[1]
    const max_lat = screen2lnglat([ 0, 0 ])[1]
    if (min_lat < 0 && max_lat > 0) {
        return (kmToPixelAtLat(km, min_lat / 2) + kmToPixelAtLat(km, max_lat / 2)) / 2
    }
    return kmToPixelAtLat(km, (min_lat + max_lat) / 2)
}


function kmToPixelAtLat(km, lat) {
    return km * navState.w * navState.scale / (2 * Math.PI * Math.cos(lat * Math.PI / 180) * 6_371)
}


function isAreaOnScreen(area) {
    const [ min_lng, max_lat ] = screen2lnglat([ 0, 0 ])
    const [ max_lng, min_lat ] = screen2lnglat([ navState.w, navState.h ])
    return doRectsIntersect(area, {
        min_lat: min_lat,
        max_lat: max_lat,
        min_lng: min_lng,
        max_lng: max_lng
    })
}


function isPointOnScreen(point) {
    const [ min_lng, max_lat ] = screen2lnglat([ 0, 0 ])
    const [ max_lng, min_lat ] = screen2lnglat([ navState.w, navState.h ])
    return min_lng <= point[0] && point[0] <= max_lng &&
        min_lat <= point[1] && point[1] <= max_lat
}


function doRectsIntersect(a, b) {
    const latIntersects = !(a.max_lat < b.min_lat || a.min_lat > b.max_lat)
    const lngIntersects = !(a.max_lng < b.min_lng || a.min_lng > b.max_lng)
    return latIntersects && lngIntersects
}


function getPopulationClass(population) {
    for (let popClassInd in drawCfg.populationClasses) {
        if (population > drawCfg.populationClasses[popClassInd]) {
            return popClassInd
        }
    }
    return drawCfg.populationClasses.length
}
