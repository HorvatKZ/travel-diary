export const navState = {
    w: 1,
    h: 1,
    scale: 1,
    minScale: 1,
    maxScale: 500,
    scalingFactor: 1.1,
    translation: {
        x: 0,
        y: 0
    },
    clicked: false,
    mouse: [ 0, 0 ]
}


export function lnglat2screen(point) {
    return [
        ((point[0] + 180) * navState.w / 360 - navState.translation.x) * navState.scale,
        (lat2mercator(point[1], navState) - navState.translation.y) * navState.scale
    ]
}


export function screen2lnglat(point) {
    return [
        (point[0] / navState.scale + navState.translation.x) * 360 / navState.w - 180,
        mercator2lat(point[1] / navState.scale + navState.translation.y, navState)
    ]
}


export function onWheel(e) {
    const prevScale = navState.scale
    if (e.wheelDelta > 0) {
        navState.scale *= navState.scalingFactor
    } else {
        navState.scale /= navState.scalingFactor
    }
    navState.scale = clamp(navState.scale, navState.minScale, navState.maxScale)

    navState.translation.x += e.offsetX * (1 / prevScale - 1 / navState.scale)
    navState.translation.y += e.offsetY * (1 / prevScale - 1 / navState.scale)
    clampTranslation()
}


export function onMouseDown(e) {
    if (e.button == 0) {
        navState.clicked = true
    }
}


export function onMouseUp(e) {
    if (e.button == 0) {
        navState.clicked = false
    }
}


export function onMouseMove(e) {
    navState.mouse = [ e.offsetX, e.offsetY ]

    if (navState.clicked) {
        navState.translation.x -= e.movementX / navState.scale
        navState.translation.y -= e.movementY / navState.scale
        clampTranslation()
        return true
    }

    return false
}


function lat2mercator(lat, navState) {
    return (Math.log(Math.tan(Math.PI / 4 + (lat * Math.PI) / 360)) - Math.PI) / (2 * Math.PI) * -navState.w
}


function mercator2lat(mer, navState) {
    return (2 * Math.atan(Math.exp(mer / -navState.w * 2 * Math.PI + Math.PI)) - Math.PI / 2) * 180 / Math.PI 
}


function clampTranslation() {
    navState.translation.x = clamp(navState.translation.x, 0, navState.w * (1 - 1 / navState.scale))
    navState.translation.y = clamp(navState.translation.y, 0, navState.h * (1 - 1 / navState.scale) + (navState.w - navState.h))
}


function clamp(a, min, max) {
    if (a < min) {
        return min
    } else if (a > max) {
        return max
    } else {
        return a
    }
}