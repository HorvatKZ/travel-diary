export default
{
    "ZWE": {
        "name": "Zimbabwe",
        "formal": "Republic of Zimbabwe",
        "population": 14645468,
        "flag": "\ud83c\uddff\ud83c\uddfc"
    },
    "ZMB": {
        "name": "Zambia",
        "formal": "Republic of Zambia",
        "population": 17861030,
        "flag": "\ud83c\uddff\ud83c\uddf2"
    },
    "YEM": {
        "name": "Yemen",
        "formal": "Republic of Yemen",
        "population": 29161922,
        "flag": "\ud83c\uddfe\ud83c\uddea"
    },
    "VNM": {
        "name": "Vietnam",
        "formal": "Socialist Republic of Vietnam",
        "population": 96462106,
        "flag": "\ud83c\uddfb\ud83c\uddf3"
    },
    "VEN": {
        "name": "Venezuela",
        "formal": "Bolivarian Republic of Venezuela",
        "population": 28515829,
        "flag": "\ud83c\uddfb\ud83c\uddea"
    },
    "VAT": {
        "name": "Vatican",
        "formal": "State of the Vatican City",
        "population": 825,
        "flag": "\ud83c\uddfb\ud83c\udde6"
    },
    "VUT": {
        "name": "Vanuatu",
        "formal": "Republic of Vanuatu",
        "population": 299882,
        "flag": "\ud83c\uddfb\ud83c\uddfa"
    },
    "UZB": {
        "name": "Uzbekistan",
        "formal": "Republic of Uzbekistan",
        "population": 33580650,
        "flag": "\ud83c\uddfa\ud83c\uddff"
    },
    "URY": {
        "name": "Uruguay",
        "formal": "Oriental Republic of Uruguay",
        "population": 3461734,
        "flag": "\ud83c\uddfa\ud83c\uddfe"
    },
    "FSM": {
        "name": "Micronesia",
        "formal": "Federated States of Micronesia",
        "population": 113815,
        "flag": "\ud83c\uddeb\ud83c\uddf2"
    },
    "MHL": {
        "name": "Marshall Is.",
        "formal": "Republic of the Marshall Islands",
        "population": 58791,
        "flag": "\ud83c\uddf2\ud83c\udded"
    },
    "MNP": {
        "name": "N. Mariana Is.",
        "formal": "Commonwealth of the Northern Mariana Islands",
        "population": 57216,
        "flag": "\ud83c\uddf2\ud83c\uddf5"
    },
    "VIR": {
        "name": "U.S. Virgin Is.",
        "formal": "Virgin Islands of the United States",
        "population": 106631,
        "flag": "\ud83c\uddfb\ud83c\uddee"
    },
    "GUM": {
        "name": "Guam",
        "formal": "Territory of Guam",
        "population": 167294,
        "flag": "\ud83c\uddec\ud83c\uddfa"
    },
    "ASM": {
        "name": "American Samoa",
        "formal": "American Samoa",
        "population": 55312,
        "flag": "\ud83c\udde6\ud83c\uddf8"
    },
    "PRI": {
        "name": "Puerto Rico",
        "formal": "Commonwealth of Puerto Rico",
        "population": 3193694,
        "flag": "\ud83c\uddf5\ud83c\uddf7"
    },
    "USA": {
        "name": "United States of America",
        "formal": "United States of America",
        "population": 328239523,
        "flag": "\ud83c\uddfa\ud83c\uddf8"
    },
    "SGS": {
        "name": "S. Geo. and the Is.",
        "formal": "South Georgia and the Islands",
        "population": 30,
        "flag": "\ud83c\uddec\ud83c\uddf8"
    },
    "IOT": {
        "name": "Br. Indian Ocean Ter.",
        "formal": "Br. Indian Ocean Ter.",
        "population": 3000,
        "flag": "\ud83c\uddee\ud83c\uddf4"
    },
    "SHN": {
        "name": "Saint Helena",
        "formal": "Saint Helena",
        "population": 4534,
        "flag": "\ud83c\uddf8\ud83c\udded"
    },
    "PCN": {
        "name": "Pitcairn Is.",
        "formal": "Pitcairn, Henderson, Ducie and Oeno Islands",
        "population": 54,
        "flag": "\ud83c\uddf5\ud83c\uddf3"
    },
    "AIA": {
        "name": "Anguilla",
        "formal": "Anguilla",
        "population": 14731,
        "flag": "\ud83c\udde6\ud83c\uddee"
    },
    "FLK": {
        "name": "Falkland Is.",
        "formal": "Falkland Islands",
        "population": 3398,
        "flag": "\ud83c\uddeb\ud83c\uddf0"
    },
    "CYM": {
        "name": "Cayman Is.",
        "formal": "Cayman Islands",
        "population": 64948,
        "flag": "\ud83c\uddf0\ud83c\uddfe"
    },
    "BMU": {
        "name": "Bermuda",
        "formal": "The Bermudas or Somers Isles",
        "population": 63918,
        "flag": "\ud83c\udde7\ud83c\uddf2"
    },
    "VGB": {
        "name": "British Virgin Is.",
        "formal": "British Virgin Islands",
        "population": 30030,
        "flag": "\ud83c\uddfb\ud83c\uddec"
    },
    "TCA": {
        "name": "Turks and Caicos Is.",
        "formal": "Turks and Caicos Islands",
        "population": 38191,
        "flag": "\ud83c\uddf9\ud83c\udde8"
    },
    "MSR": {
        "name": "Montserrat",
        "formal": "Montserrat",
        "population": 4649,
        "flag": "\ud83c\uddf2\ud83c\uddf8"
    },
    "JEY": {
        "name": "Jersey",
        "formal": "Bailiwick of Jersey",
        "population": 107800,
        "flag": "\ud83c\uddef\ud83c\uddea"
    },
    "GGY": {
        "name": "Guernsey",
        "formal": "Bailiwick of Guernsey",
        "population": 62792,
        "flag": "\ud83c\uddec\ud83c\uddec"
    },
    "IMN": {
        "name": "Isle of Man",
        "formal": "Isle of Man",
        "population": 84584,
        "flag": "\ud83c\uddee\ud83c\uddf2"
    },
    "GBR": {
        "name": "United Kingdom",
        "formal": "United Kingdom of Great Britain and Northern Ireland",
        "population": 66834405,
        "flag": "\ud83c\uddec\ud83c\udde7"
    },
    "ARE": {
        "name": "United Arab Emirates",
        "formal": "United Arab Emirates",
        "population": 9770529,
        "flag": "\ud83c\udde6\ud83c\uddea"
    },
    "UKR": {
        "name": "Ukraine",
        "formal": "Ukraine",
        "population": 44385155,
        "flag": "\ud83c\uddfa\ud83c\udde6"
    },
    "UGA": {
        "name": "Uganda",
        "formal": "Republic of Uganda",
        "population": 44269594,
        "flag": "\ud83c\uddfa\ud83c\uddec"
    },
    "TKM": {
        "name": "Turkmenistan",
        "formal": "Turkmenistan",
        "population": 5942089,
        "flag": "\ud83c\uddf9\ud83c\uddf2"
    },
    "TUR": {
        "name": "Turkey",
        "formal": "Republic of Turkey",
        "population": 83429615,
        "flag": "\ud83c\uddf9\ud83c\uddf7"
    },
    "TUN": {
        "name": "Tunisia",
        "formal": "Republic of Tunisia",
        "population": 11694719,
        "flag": "\ud83c\uddf9\ud83c\uddf3"
    },
    "TTO": {
        "name": "Trinidad and Tobago",
        "formal": "Republic of Trinidad and Tobago",
        "population": 1394973,
        "flag": "\ud83c\uddf9\ud83c\uddf9"
    },
    "TON": {
        "name": "Tonga",
        "formal": "Kingdom of Tonga",
        "population": 104494,
        "flag": "\ud83c\uddf9\ud83c\uddf4"
    },
    "TGO": {
        "name": "Togo",
        "formal": "Togolese Republic",
        "population": 8082366,
        "flag": "\ud83c\uddf9\ud83c\uddec"
    },
    "TLS": {
        "name": "Timor-Leste",
        "formal": "Democratic Republic of Timor-Leste",
        "population": 1293119,
        "flag": "\ud83c\uddf9\ud83c\uddf1"
    },
    "THA": {
        "name": "Thailand",
        "formal": "Kingdom of Thailand",
        "population": 69625582,
        "flag": "\ud83c\uddf9\ud83c\udded"
    },
    "TZA": {
        "name": "Tanzania",
        "formal": "United Republic of Tanzania",
        "population": 58005463,
        "flag": "\ud83c\uddf9\ud83c\uddff"
    },
    "TJK": {
        "name": "Tajikistan",
        "formal": "Republic of Tajikistan",
        "population": 9321018,
        "flag": "\ud83c\uddf9\ud83c\uddef"
    },
    "TWN": {
        "name": "Taiwan",
        "formal": "Taiwan",
        "population": 23568378,
        "flag": "\ud83c\uddf9\ud83c\uddfc"
    },
    "SYR": {
        "name": "Syria",
        "formal": "Syrian Arab Republic",
        "population": 17070135,
        "flag": "\ud83c\uddf8\ud83c\uddfe"
    },
    "CHE": {
        "name": "Switzerland",
        "formal": "Swiss Confederation",
        "population": 8574832,
        "flag": "\ud83c\udde8\ud83c\udded"
    },
    "SWE": {
        "name": "Sweden",
        "formal": "Kingdom of Sweden",
        "population": 10285453,
        "flag": "\ud83c\uddf8\ud83c\uddea"
    },
    "SWZ": {
        "name": "eSwatini",
        "formal": "Kingdom of eSwatini",
        "population": 1148130,
        "flag": "\ud83c\uddf8\ud83c\uddff"
    },
    "SUR": {
        "name": "Suriname",
        "formal": "Republic of Suriname",
        "population": 581363,
        "flag": "\ud83c\uddf8\ud83c\uddf7"
    },
    "SDS": {
        "name": "S. Sudan",
        "formal": "Republic of South Sudan",
        "population": 11062113,
        "flag": "\ud83c\uddf8\ud83c\uddf8"
    },
    "SDN": {
        "name": "Sudan",
        "formal": "Republic of the Sudan",
        "population": 42813238,
        "flag": "\ud83c\uddf8\ud83c\udde9"
    },
    "LKA": {
        "name": "Sri Lanka",
        "formal": "Democratic Socialist Republic of Sri Lanka",
        "population": 21803000,
        "flag": "\ud83c\uddf1\ud83c\uddf0"
    },
    "ESP": {
        "name": "Spain",
        "formal": "Kingdom of Spain",
        "population": 47076781,
        "flag": "\ud83c\uddea\ud83c\uddf8"
    },
    "KOR": {
        "name": "South Korea",
        "formal": "Republic of Korea",
        "population": 51709098,
        "flag": "\ud83c\uddf0\ud83c\uddf7"
    },
    "ZAF": {
        "name": "South Africa",
        "formal": "Republic of South Africa",
        "population": 58558270,
        "flag": "\ud83c\uddff\ud83c\udde6"
    },
    "SOM": {
        "name": "Somalia",
        "formal": "Federal Republic of Somalia",
        "population": 10192317.3,
        "flag": "\ud83c\uddf8\ud83c\uddf4"
    },
    "SOL": {
        "name": "Somaliland",
        "formal": "Republic of Somaliland",
        "population": 5096159,
        "flag": ""
    },
    "SLB": {
        "name": "Solomon Is.",
        "formal": "Solomon Is.",
        "population": 669823,
        "flag": "\ud83c\uddf8\ud83c\udde7"
    },
    "SVK": {
        "name": "Slovakia",
        "formal": "Slovak Republic",
        "population": 5454073,
        "flag": "\ud83c\uddf8\ud83c\uddf0"
    },
    "SVN": {
        "name": "Slovenia",
        "formal": "Republic of Slovenia",
        "population": 2087946,
        "flag": "\ud83c\uddf8\ud83c\uddee"
    },
    "SGP": {
        "name": "Singapore",
        "formal": "Republic of Singapore",
        "population": 5703569,
        "flag": "\ud83c\uddf8\ud83c\uddec"
    },
    "SLE": {
        "name": "Sierra Leone",
        "formal": "Republic of Sierra Leone",
        "population": 7813215,
        "flag": "\ud83c\uddf8\ud83c\uddf1"
    },
    "SYC": {
        "name": "Seychelles",
        "formal": "Republic of Seychelles",
        "population": 97625,
        "flag": "\ud83c\uddf8\ud83c\udde8"
    },
    "SRB": {
        "name": "Serbia",
        "formal": "Republic of Serbia",
        "population": 6944975,
        "flag": "\ud83c\uddf7\ud83c\uddf8"
    },
    "SEN": {
        "name": "Senegal",
        "formal": "Republic of Senegal",
        "population": 16296364,
        "flag": "\ud83c\uddf8\ud83c\uddf3"
    },
    "SAU": {
        "name": "Saudi Arabia",
        "formal": "Kingdom of Saudi Arabia",
        "population": 34268528,
        "flag": "\ud83c\uddf8\ud83c\udde6"
    },
    "STP": {
        "name": "S\u00e3o Tom\u00e9 and Principe",
        "formal": "Democratic Republic of S\u00e3o Tom\u00e9 and Principe",
        "population": 215056,
        "flag": "\ud83c\uddf8\ud83c\uddf9"
    },
    "SMR": {
        "name": "San Marino",
        "formal": "Republic of San Marino",
        "population": 33860,
        "flag": "\ud83c\uddf8\ud83c\uddf2"
    },
    "WSM": {
        "name": "Samoa",
        "formal": "Independent State of Samoa",
        "population": 197097,
        "flag": "\ud83c\uddfc\ud83c\uddf8"
    },
    "VCT": {
        "name": "St. Vin. and Gren.",
        "formal": "Saint Vincent and the Grenadines",
        "population": 110589,
        "flag": "\ud83c\uddfb\ud83c\udde8"
    },
    "LCA": {
        "name": "Saint Lucia",
        "formal": "Saint Lucia",
        "population": 182790,
        "flag": "\ud83c\uddf1\ud83c\udde8"
    },
    "KNA": {
        "name": "St. Kitts and Nevis",
        "formal": "Federation of Saint Kitts and Nevis",
        "population": 52834,
        "flag": "\ud83c\uddf0\ud83c\uddf3"
    },
    "RWA": {
        "name": "Rwanda",
        "formal": "Republic of Rwanda",
        "population": 12626950,
        "flag": "\ud83c\uddf7\ud83c\uddfc"
    },
    "RUS": {
        "name": "Russia",
        "formal": "Russian Federation",
        "population": 144373535,
        "flag": "\ud83c\uddf7\ud83c\uddfa"
    },
    "ROU": {
        "name": "Romania",
        "formal": "Romania",
        "population": 19356544,
        "flag": "\ud83c\uddf7\ud83c\uddf4"
    },
    "QAT": {
        "name": "Qatar",
        "formal": "State of Qatar",
        "population": 2832067,
        "flag": "\ud83c\uddf6\ud83c\udde6"
    },
    "PRT": {
        "name": "Portugal",
        "formal": "Portuguese Republic",
        "population": 10269417,
        "flag": "\ud83c\uddf5\ud83c\uddf9"
    },
    "POL": {
        "name": "Poland",
        "formal": "Republic of Poland",
        "population": 37970874,
        "flag": "\ud83c\uddf5\ud83c\uddf1"
    },
    "PHL": {
        "name": "Philippines",
        "formal": "Republic of the Philippines",
        "population": 108116615,
        "flag": "\ud83c\uddf5\ud83c\udded"
    },
    "PER": {
        "name": "Peru",
        "formal": "Republic of Peru",
        "population": 32510453,
        "flag": "\ud83c\uddf5\ud83c\uddea"
    },
    "PRY": {
        "name": "Paraguay",
        "formal": "Republic of Paraguay",
        "population": 7044636,
        "flag": "\ud83c\uddf5\ud83c\uddfe"
    },
    "PNG": {
        "name": "Papua New Guinea",
        "formal": "Independent State of Papua New Guinea",
        "population": 8776109,
        "flag": "\ud83c\uddf5\ud83c\uddec"
    },
    "PAN": {
        "name": "Panama",
        "formal": "Republic of Panama",
        "population": 4246439,
        "flag": "\ud83c\uddf5\ud83c\udde6"
    },
    "PLW": {
        "name": "Palau",
        "formal": "Republic of Palau",
        "population": 18008,
        "flag": "\ud83c\uddf5\ud83c\uddfc"
    },
    "PAK": {
        "name": "Pakistan",
        "formal": "Islamic Republic of Pakistan",
        "population": 216565318,
        "flag": "\ud83c\uddf5\ud83c\uddf0"
    },
    "OMN": {
        "name": "Oman",
        "formal": "Sultanate of Oman",
        "population": 4974986,
        "flag": "\ud83c\uddf4\ud83c\uddf2"
    },
    "NOR": {
        "name": "Norway",
        "formal": "Kingdom of Norway",
        "population": 5347896,
        "flag": "\ud83c\uddf3\ud83c\uddf4"
    },
    "PRK": {
        "name": "North Korea",
        "formal": "Democratic People's Republic of Korea",
        "population": 25666161,
        "flag": "\ud83c\uddf0\ud83c\uddf5"
    },
    "NGA": {
        "name": "Nigeria",
        "formal": "Federal Republic of Nigeria",
        "population": 200963599,
        "flag": "\ud83c\uddf3\ud83c\uddec"
    },
    "NER": {
        "name": "Niger",
        "formal": "Republic of Niger",
        "population": 23310715,
        "flag": "\ud83c\uddf3\ud83c\uddea"
    },
    "NIC": {
        "name": "Nicaragua",
        "formal": "Republic of Nicaragua",
        "population": 6545502,
        "flag": "\ud83c\uddf3\ud83c\uddee"
    },
    "NZL": {
        "name": "New Zealand",
        "formal": "New Zealand",
        "population": 4917000,
        "flag": "\ud83c\uddf3\ud83c\uddff"
    },
    "NIU": {
        "name": "Niue",
        "formal": "Niue",
        "population": 1620,
        "flag": "\ud83c\uddf3\ud83c\uddfa"
    },
    "COK": {
        "name": "Cook Is.",
        "formal": "Cook Is.",
        "population": 17459,
        "flag": "\ud83c\udde8\ud83c\uddf0"
    },
    "NLD": {
        "name": "Netherlands",
        "formal": "Kingdom of the Netherlands",
        "population": 17332850,
        "flag": "\ud83c\uddf3\ud83c\uddf1"
    },
    "ABW": {
        "name": "Aruba",
        "formal": "Aruba",
        "population": 106314,
        "flag": "\ud83c\udde6\ud83c\uddfc"
    },
    "CUW": {
        "name": "Cura\u00e7ao",
        "formal": "Cura\u00e7ao",
        "population": 157538,
        "flag": "\ud83c\udde8\ud83c\uddfc"
    },
    "NPL": {
        "name": "Nepal",
        "formal": "Nepal",
        "population": 28608710,
        "flag": "\ud83c\uddf3\ud83c\uddf5"
    },
    "NRU": {
        "name": "Nauru",
        "formal": "Republic of Nauru",
        "population": 12581,
        "flag": "\ud83c\uddf3\ud83c\uddf7"
    },
    "NAM": {
        "name": "Namibia",
        "formal": "Republic of Namibia",
        "population": 2494530,
        "flag": "\ud83c\uddf3\ud83c\udde6"
    },
    "MOZ": {
        "name": "Mozambique",
        "formal": "Republic of Mozambique",
        "population": 30366036,
        "flag": "\ud83c\uddf2\ud83c\uddff"
    },
    "MAR": {
        "name": "Morocco",
        "formal": "Kingdom of Morocco",
        "population": 36471769,
        "flag": "\ud83c\uddf2\ud83c\udde6"
    },
    "SAH": {
        "name": "W. Sahara",
        "formal": "Sahrawi Arab Democratic Republic",
        "population": 603253,
        "flag": "\ud83c\uddea\ud83c\udded"
    },
    "MNE": {
        "name": "Montenegro",
        "formal": "Montenegro",
        "population": 622137,
        "flag": "\ud83c\uddf2\ud83c\uddea"
    },
    "MNG": {
        "name": "Mongolia",
        "formal": "Mongolia",
        "population": 3225167,
        "flag": "\ud83c\uddf2\ud83c\uddf3"
    },
    "MDA": {
        "name": "Moldova",
        "formal": "Republic of Moldova",
        "population": 2657637,
        "flag": "\ud83c\uddf2\ud83c\udde9"
    },
    "MCO": {
        "name": "Monaco",
        "formal": "Principality of Monaco",
        "population": 38964,
        "flag": "\ud83c\uddf2\ud83c\udde8"
    },
    "MEX": {
        "name": "Mexico",
        "formal": "United Mexican States",
        "population": 127575529,
        "flag": "\ud83c\uddf2\ud83c\uddfd"
    },
    "MUS": {
        "name": "Mauritius",
        "formal": "Republic of Mauritius",
        "population": 1265711,
        "flag": "\ud83c\uddf2\ud83c\uddfa"
    },
    "MRT": {
        "name": "Mauritania",
        "formal": "Islamic Republic of Mauritania",
        "population": 4525696,
        "flag": "\ud83c\uddf2\ud83c\uddf7"
    },
    "MLT": {
        "name": "Malta",
        "formal": "Republic of Malta",
        "population": 502653,
        "flag": "\ud83c\uddf2\ud83c\uddf9"
    },
    "MLI": {
        "name": "Mali",
        "formal": "Republic of Mali",
        "population": 19658031,
        "flag": "\ud83c\uddf2\ud83c\uddf1"
    },
    "MDV": {
        "name": "Maldives",
        "formal": "Republic of Maldives",
        "population": 530953,
        "flag": "\ud83c\uddf2\ud83c\uddfb"
    },
    "MYS": {
        "name": "Malaysia",
        "formal": "Malaysia",
        "population": 31949777,
        "flag": "\ud83c\uddf2\ud83c\uddfe"
    },
    "MWI": {
        "name": "Malawi",
        "formal": "Republic of Malawi",
        "population": 18628747,
        "flag": "\ud83c\uddf2\ud83c\uddfc"
    },
    "MDG": {
        "name": "Madagascar",
        "formal": "Republic of Madagascar",
        "population": 26969307,
        "flag": "\ud83c\uddf2\ud83c\uddec"
    },
    "MKD": {
        "name": "North Macedonia",
        "formal": "Republic of North Macedonia",
        "population": 2083459,
        "flag": "\ud83c\uddf2\ud83c\uddf0"
    },
    "LUX": {
        "name": "Luxembourg",
        "formal": "Grand Duchy of Luxembourg",
        "population": 619896,
        "flag": "\ud83c\uddf1\ud83c\uddfa"
    },
    "LTU": {
        "name": "Lithuania",
        "formal": "Republic of Lithuania",
        "population": 2786844,
        "flag": "\ud83c\uddf1\ud83c\uddf9"
    },
    "LIE": {
        "name": "Liechtenstein",
        "formal": "Principality of Liechtenstein",
        "population": 38019,
        "flag": "\ud83c\uddf1\ud83c\uddee"
    },
    "LBY": {
        "name": "Libya",
        "formal": "Libya",
        "population": 6777452,
        "flag": "\ud83c\uddf1\ud83c\uddfe"
    },
    "LBR": {
        "name": "Liberia",
        "formal": "Republic of Liberia",
        "population": 4937374,
        "flag": "\ud83c\uddf1\ud83c\uddf7"
    },
    "LSO": {
        "name": "Lesotho",
        "formal": "Kingdom of Lesotho",
        "population": 2125268,
        "flag": "\ud83c\uddf1\ud83c\uddf8"
    },
    "LBN": {
        "name": "Lebanon",
        "formal": "Lebanese Republic",
        "population": 6855713,
        "flag": "\ud83c\uddf1\ud83c\udde7"
    },
    "LVA": {
        "name": "Latvia",
        "formal": "Republic of Latvia",
        "population": 1912789,
        "flag": "\ud83c\uddf1\ud83c\uddfb"
    },
    "LAO": {
        "name": "Laos",
        "formal": "Lao People's Democratic Republic",
        "population": 7169455,
        "flag": "\ud83c\uddf1\ud83c\udde6"
    },
    "KGZ": {
        "name": "Kyrgyzstan",
        "formal": "Kyrgyz Republic",
        "population": 6456900,
        "flag": "\ud83c\uddf0\ud83c\uddec"
    },
    "KWT": {
        "name": "Kuwait",
        "formal": "State of Kuwait",
        "population": 4207083,
        "flag": "\ud83c\uddf0\ud83c\uddfc"
    },
    "KOS": {
        "name": "Kosovo",
        "formal": "Republic of Kosovo",
        "population": 1794248,
        "flag": "\ud83c\uddfd\ud83c\uddf0"
    },
    "KIR": {
        "name": "Kiribati",
        "formal": "Republic of Kiribati",
        "population": 117606,
        "flag": "\ud83c\uddf0\ud83c\uddee"
    },
    "KEN": {
        "name": "Kenya",
        "formal": "Republic of Kenya",
        "population": 52573973,
        "flag": "\ud83c\uddf0\ud83c\uddea"
    },
    "KAZ": {
        "name": "Kazakhstan",
        "formal": "Republic of Kazakhstan",
        "population": 18513930,
        "flag": "\ud83c\uddf0\ud83c\uddff"
    },
    "JOR": {
        "name": "Jordan",
        "formal": "Hashemite Kingdom of Jordan",
        "population": 10101694,
        "flag": "\ud83c\uddef\ud83c\uddf4"
    },
    "JPN": {
        "name": "Japan",
        "formal": "Japan",
        "population": 126264931,
        "flag": "\ud83c\uddef\ud83c\uddf5"
    },
    "JAM": {
        "name": "Jamaica",
        "formal": "Jamaica",
        "population": 2948279,
        "flag": "\ud83c\uddef\ud83c\uddf2"
    },
    "ITA": {
        "name": "Italy",
        "formal": "Italian Republic",
        "population": 60297396,
        "flag": "\ud83c\uddee\ud83c\uddf9"
    },
    "ISR": {
        "name": "Israel",
        "formal": "State of Israel",
        "population": 9053300,
        "flag": "\ud83c\uddee\ud83c\uddf1"
    },
    "PSX": {
        "name": "Palestine",
        "formal": "West Bank and Gaza",
        "population": 4685306,
        "flag": "\ud83c\uddf5\ud83c\uddf8"
    },
    "IRL": {
        "name": "Ireland",
        "formal": "Ireland",
        "population": 4941444,
        "flag": "\ud83c\uddee\ud83c\uddea"
    },
    "IRQ": {
        "name": "Iraq",
        "formal": "Republic of Iraq",
        "population": 39309783,
        "flag": "\ud83c\uddee\ud83c\uddf6"
    },
    "IRN": {
        "name": "Iran",
        "formal": "Islamic Republic of Iran",
        "population": 82913906,
        "flag": "\ud83c\uddee\ud83c\uddf7"
    },
    "IDN": {
        "name": "Indonesia",
        "formal": "Republic of Indonesia",
        "population": 270625568,
        "flag": "\ud83c\uddee\ud83c\udde9"
    },
    "IND": {
        "name": "India",
        "formal": "Republic of India",
        "population": 1366417754,
        "flag": "\ud83c\uddee\ud83c\uddf3"
    },
    "ISL": {
        "name": "Iceland",
        "formal": "Republic of Iceland",
        "population": 361313,
        "flag": "\ud83c\uddee\ud83c\uddf8"
    },
    "HUN": {
        "name": "Hungary",
        "formal": "Republic of Hungary",
        "population": 9769949,
        "flag": "\ud83c\udded\ud83c\uddfa"
    },
    "HND": {
        "name": "Honduras",
        "formal": "Republic of Honduras",
        "population": 9746117,
        "flag": "\ud83c\udded\ud83c\uddf3"
    },
    "HTI": {
        "name": "Haiti",
        "formal": "Republic of Haiti",
        "population": 11263077,
        "flag": "\ud83c\udded\ud83c\uddf9"
    },
    "GUY": {
        "name": "Guyana",
        "formal": "Co-operative Republic of Guyana",
        "population": 782766,
        "flag": "\ud83c\uddec\ud83c\uddfe"
    },
    "GNB": {
        "name": "Guinea-Bissau",
        "formal": "Republic of Guinea-Bissau",
        "population": 1920922,
        "flag": "\ud83c\uddec\ud83c\uddfc"
    },
    "GIN": {
        "name": "Guinea",
        "formal": "Republic of Guinea",
        "population": 12771246,
        "flag": "\ud83c\uddec\ud83c\uddf3"
    },
    "GTM": {
        "name": "Guatemala",
        "formal": "Republic of Guatemala",
        "population": 16604026,
        "flag": "\ud83c\uddec\ud83c\uddf9"
    },
    "GRD": {
        "name": "Grenada",
        "formal": "Grenada",
        "population": 112003,
        "flag": "\ud83c\uddec\ud83c\udde9"
    },
    "GRC": {
        "name": "Greece",
        "formal": "Hellenic Republic",
        "population": 10716322,
        "flag": "\ud83c\uddec\ud83c\uddf7"
    },
    "GHA": {
        "name": "Ghana",
        "formal": "Republic of Ghana",
        "population": 30417856,
        "flag": "\ud83c\uddec\ud83c\udded"
    },
    "DEU": {
        "name": "Germany",
        "formal": "Federal Republic of Germany",
        "population": 83132799,
        "flag": "\ud83c\udde9\ud83c\uddea"
    },
    "GEO": {
        "name": "Georgia",
        "formal": "Georgia",
        "population": 3720382,
        "flag": "\ud83c\uddec\ud83c\uddea"
    },
    "GMB": {
        "name": "Gambia",
        "formal": "Republic of the Gambia",
        "population": 2347706,
        "flag": "\ud83c\uddec\ud83c\uddf2"
    },
    "GAB": {
        "name": "Gabon",
        "formal": "Gabonese Republic",
        "population": 2172579,
        "flag": "\ud83c\uddec\ud83c\udde6"
    },
    "FRA": {
        "name": "France",
        "formal": "French Republic",
        "population": 67059887,
        "flag": "\ud83c\uddeb\ud83c\uddf7"
    },
    "SPM": {
        "name": "St. Pierre and Miquelon",
        "formal": "Saint Pierre and Miquelon",
        "population": 5997,
        "flag": "\ud83c\uddf5\ud83c\uddf2"
    },
    "WLF": {
        "name": "Wallis and Futuna Is.",
        "formal": "Wallis and Futuna Islands",
        "population": 11558,
        "flag": "\ud83c\uddfc\ud83c\uddeb"
    },
    "MAF": {
        "name": "St-Martin",
        "formal": "Saint-Martin (French part)",
        "population": 38002,
        "flag": "\ud83c\uddf2\ud83c\uddeb"
    },
    "BLM": {
        "name": "St-Barth\u00e9lemy",
        "formal": "Saint-Barth\u00e9lemy",
        "population": 9961,
        "flag": "\ud83c\udde7\ud83c\uddf1"
    },
    "PYF": {
        "name": "Fr. Polynesia",
        "formal": "French Polynesia",
        "population": 279287,
        "flag": "\ud83c\uddf5\ud83c\uddeb"
    },
    "NCL": {
        "name": "New Caledonia",
        "formal": "New Caledonia",
        "population": 287800,
        "flag": "\ud83c\uddf3\ud83c\udde8"
    },
    "ATF": {
        "name": "Fr. S. Antarctic Lands",
        "formal": "Territory of the French Southern and Antarctic Lands",
        "population": 140,
        "flag": "\ud83c\uddf9\ud83c\uddeb"
    },
    "ALD": {
        "name": "\u00c5land",
        "formal": "\u00c5land Islands",
        "population": 29884,
        "flag": "\ud83c\udde6\ud83c\uddfd"
    },
    "FIN": {
        "name": "Finland",
        "formal": "Republic of Finland",
        "population": 5520314,
        "flag": "\ud83c\uddeb\ud83c\uddee"
    },
    "FJI": {
        "name": "Fiji",
        "formal": "Republic of Fiji",
        "population": 889953,
        "flag": "\ud83c\uddeb\ud83c\uddef"
    },
    "ETH": {
        "name": "Ethiopia",
        "formal": "Federal Democratic Republic of Ethiopia",
        "population": 112078730,
        "flag": "\ud83c\uddea\ud83c\uddf9"
    },
    "EST": {
        "name": "Estonia",
        "formal": "Republic of Estonia",
        "population": 1326590,
        "flag": "\ud83c\uddea\ud83c\uddea"
    },
    "ERI": {
        "name": "Eritrea",
        "formal": "State of Eritrea",
        "population": 6081196,
        "flag": "\ud83c\uddea\ud83c\uddf7"
    },
    "GNQ": {
        "name": "Eq. Guinea",
        "formal": "Republic of Equatorial Guinea",
        "population": 1355986,
        "flag": "\ud83c\uddec\ud83c\uddf6"
    },
    "SLV": {
        "name": "El Salvador",
        "formal": "Republic of El Salvador",
        "population": 6453553,
        "flag": "\ud83c\uddf8\ud83c\uddfb"
    },
    "EGY": {
        "name": "Egypt",
        "formal": "Arab Republic of Egypt",
        "population": 100388073,
        "flag": "\ud83c\uddea\ud83c\uddec"
    },
    "ECU": {
        "name": "Ecuador",
        "formal": "Republic of Ecuador",
        "population": 17373662,
        "flag": "\ud83c\uddea\ud83c\udde8"
    },
    "DOM": {
        "name": "Dominican Rep.",
        "formal": "Dominican Republic",
        "population": 10738958,
        "flag": "\ud83c\udde9\ud83c\uddf4"
    },
    "DMA": {
        "name": "Dominica",
        "formal": "Commonwealth of Dominica",
        "population": 71808,
        "flag": "\ud83c\udde9\ud83c\uddf2"
    },
    "DJI": {
        "name": "Djibouti",
        "formal": "Republic of Djibouti",
        "population": 973560,
        "flag": "\ud83c\udde9\ud83c\uddef"
    },
    "GRL": {
        "name": "Greenland",
        "formal": "Greenland",
        "population": 56225,
        "flag": "\ud83c\uddec\ud83c\uddf1"
    },
    "FRO": {
        "name": "Faeroe Is.",
        "formal": "F\u00f8royar Is. (Faeroe Is.)",
        "population": 48678,
        "flag": "\ud83c\uddeb\ud83c\uddf4"
    },
    "DNK": {
        "name": "Denmark",
        "formal": "Kingdom of Denmark",
        "population": 5818553,
        "flag": "\ud83c\udde9\ud83c\uddf0"
    },
    "CZE": {
        "name": "Czechia",
        "formal": "Czech Republic",
        "population": 10669709,
        "flag": "\ud83c\udde8\ud83c\uddff"
    },
    "CYN": {
        "name": "N. Cyprus",
        "formal": "Turkish Republic of Northern Cyprus",
        "population": 326000,
        "flag": ""
    },
    "CYP": {
        "name": "Cyprus",
        "formal": "Republic of Cyprus",
        "population": 1198575,
        "flag": "\ud83c\udde8\ud83c\uddfe"
    },
    "CUB": {
        "name": "Cuba",
        "formal": "Republic of Cuba",
        "population": 11333483,
        "flag": "\ud83c\udde8\ud83c\uddfa"
    },
    "HRV": {
        "name": "Croatia",
        "formal": "Republic of Croatia",
        "population": 4067500,
        "flag": "\ud83c\udded\ud83c\uddf7"
    },
    "CIV": {
        "name": "C\u00f4te d'Ivoire",
        "formal": "Republic of Ivory Coast",
        "population": 25716544,
        "flag": "\ud83c\udde8\ud83c\uddee"
    },
    "CRI": {
        "name": "Costa Rica",
        "formal": "Republic of Costa Rica",
        "population": 5047561,
        "flag": "\ud83c\udde8\ud83c\uddf7"
    },
    "COD": {
        "name": "Dem. Rep. Congo",
        "formal": "Democratic Republic of the Congo",
        "population": 86790567,
        "flag": "\ud83c\udde8\ud83c\udde9"
    },
    "COG": {
        "name": "Congo",
        "formal": "Republic of the Congo",
        "population": 5380508,
        "flag": "\ud83c\udde8\ud83c\uddec"
    },
    "COM": {
        "name": "Comoros",
        "formal": "Union of the Comoros",
        "population": 850886,
        "flag": "\ud83c\uddf0\ud83c\uddf2"
    },
    "COL": {
        "name": "Colombia",
        "formal": "Republic of Colombia",
        "population": 50339443,
        "flag": "\ud83c\udde8\ud83c\uddf4"
    },
    "CHN": {
        "name": "China",
        "formal": "People's Republic of China",
        "population": 1397715000,
        "flag": "\ud83c\udde8\ud83c\uddf3"
    },
    "MAC": {
        "name": "Macao",
        "formal": "Macao Special Administrative Region, PRC",
        "population": 640445,
        "flag": "\ud83c\uddf2\ud83c\uddf4"
    },
    "HKG": {
        "name": "Hong Kong",
        "formal": "Hong Kong Special Administrative Region, PRC",
        "population": 7507400,
        "flag": "\ud83c\udded\ud83c\uddf0"
    },
    "CHL": {
        "name": "Chile",
        "formal": "Republic of Chile",
        "population": 18952038,
        "flag": "\ud83c\udde8\ud83c\uddf1"
    },
    "TCD": {
        "name": "Chad",
        "formal": "Republic of Chad",
        "population": 15946876,
        "flag": "\ud83c\uddf9\ud83c\udde9"
    },
    "CAF": {
        "name": "Central African Rep.",
        "formal": "Central African Republic",
        "population": 4745185,
        "flag": "\ud83c\udde8\ud83c\uddeb"
    },
    "CPV": {
        "name": "Cabo Verde",
        "formal": "Republic of Cabo Verde",
        "population": 549935,
        "flag": "\ud83c\udde8\ud83c\uddfb"
    },
    "CAN": {
        "name": "Canada",
        "formal": "Canada",
        "population": 37589262,
        "flag": "\ud83c\udde8\ud83c\udde6"
    },
    "CMR": {
        "name": "Cameroon",
        "formal": "Republic of Cameroon",
        "population": 25876380,
        "flag": "\ud83c\udde8\ud83c\uddf2"
    },
    "KHM": {
        "name": "Cambodia",
        "formal": "Kingdom of Cambodia",
        "population": 16486542,
        "flag": "\ud83c\uddf0\ud83c\udded"
    },
    "MMR": {
        "name": "Myanmar",
        "formal": "Republic of the Union of Myanmar",
        "population": 54045420,
        "flag": "\ud83c\uddf2\ud83c\uddf2"
    },
    "BDI": {
        "name": "Burundi",
        "formal": "Republic of Burundi",
        "population": 11530580,
        "flag": "\ud83c\udde7\ud83c\uddee"
    },
    "BFA": {
        "name": "Burkina Faso",
        "formal": "Burkina Faso",
        "population": 20321378,
        "flag": "\ud83c\udde7\ud83c\uddeb"
    },
    "BGR": {
        "name": "Bulgaria",
        "formal": "Republic of Bulgaria",
        "population": 6975761,
        "flag": "\ud83c\udde7\ud83c\uddec"
    },
    "BRN": {
        "name": "Brunei",
        "formal": "Negara Brunei Darussalam",
        "population": 433285,
        "flag": "\ud83c\udde7\ud83c\uddf3"
    },
    "BRA": {
        "name": "Brazil",
        "formal": "Federative Republic of Brazil",
        "population": 211049527,
        "flag": "\ud83c\udde7\ud83c\uddf7"
    },
    "BWA": {
        "name": "Botswana",
        "formal": "Republic of Botswana",
        "population": 2303697,
        "flag": "\ud83c\udde7\ud83c\uddfc"
    },
    "BIH": {
        "name": "Bosnia and Herz.",
        "formal": "Bosnia and Herzegovina",
        "population": 3301000,
        "flag": "\ud83c\udde7\ud83c\udde6"
    },
    "BOL": {
        "name": "Bolivia",
        "formal": "Plurinational State of Bolivia",
        "population": 11513100,
        "flag": "\ud83c\udde7\ud83c\uddf4"
    },
    "BTN": {
        "name": "Bhutan",
        "formal": "Kingdom of Bhutan",
        "population": 763092,
        "flag": "\ud83c\udde7\ud83c\uddf9"
    },
    "BEN": {
        "name": "Benin",
        "formal": "Republic of Benin",
        "population": 11801151,
        "flag": "\ud83c\udde7\ud83c\uddef"
    },
    "BLZ": {
        "name": "Belize",
        "formal": "Belize",
        "population": 390353,
        "flag": "\ud83c\udde7\ud83c\uddff"
    },
    "BEL": {
        "name": "Belgium",
        "formal": "Kingdom of Belgium",
        "population": 11484055,
        "flag": "\ud83c\udde7\ud83c\uddea"
    },
    "BLR": {
        "name": "Belarus",
        "formal": "Republic of Belarus",
        "population": 9466856,
        "flag": "\ud83c\udde7\ud83c\uddfe"
    },
    "BRB": {
        "name": "Barbados",
        "formal": "Barbados",
        "population": 287025,
        "flag": "\ud83c\udde7\ud83c\udde7"
    },
    "BGD": {
        "name": "Bangladesh",
        "formal": "People's Republic of Bangladesh",
        "population": 163046161,
        "flag": "\ud83c\udde7\ud83c\udde9"
    },
    "BHR": {
        "name": "Bahrain",
        "formal": "Kingdom of Bahrain",
        "population": 1641172,
        "flag": "\ud83c\udde7\ud83c\udded"
    },
    "BHS": {
        "name": "Bahamas",
        "formal": "Commonwealth of the Bahamas",
        "population": 389482,
        "flag": "\ud83c\udde7\ud83c\uddf8"
    },
    "AZE": {
        "name": "Azerbaijan",
        "formal": "Republic of Azerbaijan",
        "population": 10023318,
        "flag": "\ud83c\udde6\ud83c\uddff"
    },
    "AUT": {
        "name": "Austria",
        "formal": "Republic of Austria",
        "population": 8877067,
        "flag": "\ud83c\udde6\ud83c\uddf9"
    },
    "AUS": {
        "name": "Australia",
        "formal": "Commonwealth of Australia",
        "population": 25364307,
        "flag": "\ud83c\udde6\ud83c\uddfa"
    },
    "IOA": {
        "name": "Indian Ocean Ter.",
        "formal": "Indian Ocean Ter.",
        "population": 2387,
        "flag": "\ud83c\udde6\ud83c\uddfa"
    },
    "HMD": {
        "name": "Heard I. and McDonald Is.",
        "formal": "Territory of Heard Island and McDonald Islands",
        "population": 0,
        "flag": "\ud83c\udded\ud83c\uddf2"
    },
    "NFK": {
        "name": "Norfolk Island",
        "formal": "Territory of Norfolk Island",
        "population": 2169,
        "flag": "\ud83c\uddf3\ud83c\uddeb"
    },
    "ATC": {
        "name": "Ashmore and Cartier Is.",
        "formal": "Territory of Ashmore and Cartier Islands",
        "population": 0,
        "flag": "\ud83c\udde6\ud83c\uddfa"
    },
    "ARM": {
        "name": "Armenia",
        "formal": "Republic of Armenia",
        "population": 2957731,
        "flag": "\ud83c\udde6\ud83c\uddf2"
    },
    "ARG": {
        "name": "Argentina",
        "formal": "Argentine Republic",
        "population": 44938712,
        "flag": "\ud83c\udde6\ud83c\uddf7"
    },
    "ATG": {
        "name": "Antigua and Barb.",
        "formal": "Antigua and Barbuda",
        "population": 97118,
        "flag": "\ud83c\udde6\ud83c\uddec"
    },
    "AGO": {
        "name": "Angola",
        "formal": "People's Republic of Angola",
        "population": 31825295,
        "flag": "\ud83c\udde6\ud83c\uddf4"
    },
    "AND": {
        "name": "Andorra",
        "formal": "Principality of Andorra",
        "population": 77142,
        "flag": "\ud83c\udde6\ud83c\udde9"
    },
    "DZA": {
        "name": "Algeria",
        "formal": "People's Democratic Republic of Algeria",
        "population": 43053054,
        "flag": "\ud83c\udde9\ud83c\uddff"
    },
    "ALB": {
        "name": "Albania",
        "formal": "Republic of Albania",
        "population": 2854191,
        "flag": "\ud83c\udde6\ud83c\uddf1"
    },
    "AFG": {
        "name": "Afghanistan",
        "formal": "Islamic State of Afghanistan",
        "population": 38041754,
        "flag": "\ud83c\udde6\ud83c\uddeb"
    },
    "KAS": {
        "name": "Siachen Glacier",
        "formal": "Siachen Glacier",
        "population": 6000,
        "flag": ""
    },
    "ATA": {
        "name": "Antarctica",
        "formal": "Antarctica",
        "population": 4490,
        "flag": "\ud83c\udde6\ud83c\uddf6"
    },
    "SXM": {
        "name": "Sint Maarten",
        "formal": "Sint Maarten (Dutch part)",
        "population": 40733,
        "flag": "\ud83c\uddf8\ud83c\uddfd"
    },
    "TUV": {
        "name": "Tuvalu",
        "formal": "Tuvalu",
        "population": 11646,
        "flag": "\ud83c\uddf9\ud83c\uddfb"
    }
}