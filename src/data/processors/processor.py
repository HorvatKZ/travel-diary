import sys
import json
from typing import Callable


def process(processor: Callable[[dict], dict|None]):
    result = []

    with open(sys.argv[1]) as f:
        source = json.load(f)

    for feature in source['features']:
        processed = processor(feature)
        if processed is not None:
            result.append(processed)

    with open(sys.argv[2], 'w') as f:
        json.dump(result, f)
