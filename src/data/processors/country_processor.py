from processor import process


def process_country(country: dict) -> dict:
    coords = []
    if type(country['geometry']['coordinates'][0][0][0]) == list:
        coords = [b for a in country['geometry']['coordinates'] for b in a]
    else:
        coords = country['geometry']['coordinates']
    
    min_lat, max_lat, min_lng, max_lng = 90, -90, 180, -180
    for poly in coords:
        for coord in poly:
            min_lng = min(coord[0], min_lng)
            max_lng = max(coord[0], max_lng)
            min_lat = min(coord[1], min_lat)
            max_lat = max(coord[1], max_lat)
    
    return {
        'code': country['properties']['ADM0_A3'],
        'limits': {
            'min_lng': min_lng,
            'max_lng': max_lng,
            'min_lat': min_lat,
            'max_lat': max_lat
        },
        'coords': coords
    }


if __name__ == '__main__':
    process(process_country)
