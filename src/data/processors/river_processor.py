from processor import process


def process_river(river: dict) -> dict|None:
    if river['geometry'] is None:
        return None
    
    coords = []
    if type(river['geometry']['coordinates'][0][0]) == list:
        coords = river['geometry']['coordinates']
    else:
        coords = [river['geometry']['coordinates']]
    
    min_lat, max_lat, min_lng, max_lng = 90, -90, 180, -180
    for poly in coords:
        for coord in poly:
            min_lng = min(coord[0], min_lng)
            max_lng = max(coord[0], max_lng)
            min_lat = min(coord[1], min_lat)
            max_lat = max(coord[1], max_lat)
    
    return {
        'name': river['properties']['name'],
        'limits': {
            'min_lng': min_lng,
            'max_lng': max_lng,
            'min_lat': min_lat,
            'max_lat': max_lat
        },
        'is_river': river['properties']['featurecla'] == 'River',
        'coords': coords
    }


if __name__ == '__main__':
    process(process_river)
