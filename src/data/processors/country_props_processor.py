import sys
import json


def code2flag(code: str) -> str:
    return ''.join([chr(ord(c) + 127397) for c in code])


def process_country(country: dict) -> (str, dict):
    return (country['properties']['ADM0_A3'], {
        'name': country['properties']['NAME'],
        'formal': country['properties']['FORMAL_EN'] if len(country['properties']['FORMAL_EN']) > 0 else country['properties']['NAME'],
        'population': country['properties']['POP_EST'],
        'flag': code2flag(country['properties']['ISO_A2_EH']) if country['properties']['ISO_A2_EH'][0] != '-' else ''
    })


def main():
    result = {}

    with open(sys.argv[1]) as f:
        source = json.load(f)

    for feature in source['features']:
        processed = process_country(feature)
        result[processed[0]] = processed[1]

    result_str = json.dumps(result, indent=4)
    with open(sys.argv[2], 'w') as f:
        f.write(f'export default\n{result_str}')


if __name__ == '__main__':
    main()