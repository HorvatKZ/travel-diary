import sys
import csv
import json


def convert_level(capital: str) -> str:
    match capital:
        case 'primary':
            return 0
        case 'admin':
            return 1
        case 'minor':
            return 2
        case _:
            return 3


def get_by_header(record: list, headers: list[str], header: str):
    return record[headers.index(header)]


def process_city(record: list, headers: list[str]) -> dict:
    return {
        'name': get_by_header(record, headers, 'city'),
        'level': convert_level(get_by_header(record, headers, 'capital')),
        'population': get_by_header(record, headers, 'population'),
        'coords': [ float(get_by_header(record, headers, 'lng')), float(get_by_header(record, headers, 'lat')) ]
    }



def main():
    result = []

    with open(sys.argv[1], newline='') as f:
        csv_reader = csv.reader(f)

        headers = []
        for row in csv_reader:
            if len(headers) == 0:
                headers = row
            else:
                result.append(process_city(row, headers))

    with open(sys.argv[2], 'w') as f:
        json.dump(result, f)


if __name__ == '__main__':
    main()
