# Original datasets

## Source

The database of cities originates from simplemaps' Basic [World Cities Database](https://simplemaps.com/data/world-cities) under Creative Commons Attribution 4.0 license. No changes to the database were made

Other datasets are from [NaturalEarthData](https://www.naturalearthdata.com/) and are part of the public domain.


## Versions

- ne_10m_admin_0_countries version 5.1.1
- ne_50m_admin_0_countries version 5.1.1
- ne_10m_rivers_lake_centerlines version 5.0.0
- ne_50m_rivers_lake_centerlines version 5.0.0
- ne_10m_lakes.geojson verion 5.0.0
- ne_50m_lakes.geojson verion 5.0.0
