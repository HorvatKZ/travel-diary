run::
	@pnpm run dev


build::
	@pnpm build


process::
	@echo Processing countries...
	@python3 src/data/processors/country_processor.py src/data/original/ne_10m_admin_0_countries.geojson public/data/countries_detailed.json
	@python3 src/data/processors/country_processor.py src/data/original/ne_50m_admin_0_countries.geojson public/data/countries.json
	@python3 src/data/processors/country_props_processor.py src/data/original/ne_50m_admin_0_countries.geojson src/data/country_props.js
	@echo Processing rivers...
	@python3 src/data/processors/river_processor.py src/data/original/ne_10m_rivers_lake_centerlines.geojson public/data/rivers_detailed.json
	@python3 src/data/processors/river_processor.py src/data/original/ne_50m_rivers_lake_centerlines.geojson public/data/rivers.json
	@echo Processing lakes...
	@python3 src/data/processors/lake_processor.py src/data/original/ne_10m_lakes.geojson public/data/lakes_detailed.json
	@python3 src/data/processors/lake_processor.py src/data/original/ne_50m_lakes.geojson public/data/lakes.json
	@echo Processing cities...
	@python3 src/data/processors/city_processor.py src/data/original/worldcities.csv public/data/cities.json